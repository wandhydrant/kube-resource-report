.PHONY: test docker push

IMAGE            ?= hjacobs/kube-resource-report
VERSION          ?= $(shell git describe --tags --always --dirty)
TAG              ?= $(VERSION)

default: docker

.PHONY:
install:
	poetry install

.PHONY:
lint: install
	poetry run pre-commit run --all-files

.PHONY:
test: install lint
	poetry run coverage run --source=kube_resource_report -m py.test
	poetry run coverage report

docker:
	docker buildx create --use
	docker buildx build --rm --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" -t "$(IMAGE):latest" --platform linux/amd64,linux/arm64 .
	@echo 'Docker image $(IMAGE):$(TAG) multi-arch was build (cannot be used).'

push:
	docker buildx create --use
	docker buildx build --rm --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" -t "$(IMAGE):latest" --platform linux/amd64,linux/arm64 --push .
	@echo 'Docker image $(IMAGE):$(TAG) multi-arch can now be used.'

.PHONY: version
version:
	poetry version $(VERSION)
	sed -i 's,$(IMAGE):[0-9.]*,$(IMAGE):$(TAG),g' README.md deploy/*.yaml
	sed -i 's,version: v[0-9.]*,version: v$(VERSION),g' deploy/*.yaml

.PHONY: release
release: push version
